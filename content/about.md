+++
template = "about.html"
[extra]
my_name = "Hanze Meng"
+++

# About Me

I am a software developer with over 3 years of experience in building robust web applications integrated with deep learning pipelines. My expertise lies in a range of modern technologies including Python, and TypeScript frameworks. As a data management researcher, I have spent about 2 years building supportive relational query debugging tools involved with relational calculus and algebra. I am committed to continuous learning and staying updated with industry trends.
